import org.testng.Assert;
import org.testng.annotations.Test;

public class CalculatorTest {
    @Test
    public void addTestPositive(){
        Calculator calculator=new Calculator();
        int testadd=calculator.Add(1,1);
        Assert.assertEquals(2, testadd);
    }
    @Test
    public void addTestNegative(){
        Calculator calculator=new Calculator();
        int testadd=calculator.Add(12,-10);
        Assert.assertEquals(2, testadd);
    }

    @Test
    public void SubstractionTestPositive(){
        Calculator calculator=new Calculator();
        int testadd=calculator.Subs(5,2);
        Assert.assertEquals(3, testadd);
    }
    @Test
    public void SubstractionTestNegative(){
        Calculator calculator=new Calculator();
        int testadd=calculator.Subs(-4,4);
        Assert.assertEquals(-8, testadd);
    }

    @Test
    public void MultiplyTestPositive(){
        Calculator calculator=new Calculator();
        Double testadd=calculator.Multiply(10,8);
        Assert.assertEquals(80,(double) testadd);
    }
    @Test
    public void MultiplyTestNegative(){
        Calculator calculator=new Calculator();
        Double testadd=calculator.Multiply(10,-80);
        Assert.assertEquals(-800,(double) testadd);
    }

    @Test
    public void DivisionTestPositive(){
        Calculator calculator=new Calculator();
        float testadd=calculator.Division(40,2);
        Assert.assertEquals(20,(float) testadd);
    }

    @Test
    public void DivisionTestNegative(){
        Calculator calculator=new Calculator();
        float testadd=calculator.Division(50,-10);
        Assert.assertEquals(-5,(float) testadd);
    }







}
